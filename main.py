import jinja2
import hashlib
import os
import webapp2
from datetime import datetime
import urlparse
import logging

from google.appengine.ext import db
from google.appengine.api import search
from google.appengine.api import users
from google.appengine.api import taskqueue
from google.appengine.api import memcache
from google.appengine.ext.webapp import template

import os, string, datetime, re, wsgiref.handlers, pickle, urllib

from data import DEFAULT
from crossword import *

# Puzzle
class Puzzle(db.Model):
  name = db.StringProperty()
  show = db.BooleanProperty(default=False) # public
  user = db.UserProperty()
  rows = db.IntegerProperty(default=16)
  cols = db.IntegerProperty(default=16)
  secs = db.IntegerProperty(default=3)
  bank = db.BlobProperty()
  game = db.BlobProperty()
  date = db.DateTimeProperty(auto_now_add=True)

class BaseHandler(webapp2.RequestHandler):

    @webapp2.cached_property
    def jinja2(self):
        return jinja2.get_jinja2(app=self.app)

    def render_template(
        self,
        filename,
        template_values,
        **template_args
        ):
        template = jinja_environment.get_template(filename)
        self.response.out.write(template.render(template_values))

class StaticPageHandler(BaseHandler):

  def get(self, name):

    login = users.get_current_user()
    if login:
      values = {
        'user': login,
        'logout_url': users.create_logout_url(self.request.uri),
      }
    else:
      values = {
        'login_url': users.create_login_url(self.request.uri),
      }

    path = None
    if name == 'about':
      path = os.path.join(os.path.dirname(__file__), 'templates/about.html')
    elif name == 'profile':
      path = os.path.join(os.path.dirname(__file__), 'templates/profile.html')    
    elif name == 'links':
      path = os.path.join(os.path.dirname(__file__), 'templates/links.html')    
    elif name == 'shop':
      path = os.path.join(os.path.dirname(__file__), 'templates/news.html')    
    elif name == 'logoff':
      path = os.path.join(os.path.dirname(__file__), 'templates/logoff.html')    
    else:
      self.redirect('/')

    self.response.out.write(template.render(path, values))

# Front page
class IndexPageHandler(BaseHandler):

  def get(self):

    rows, cols, secs, bank = 16, 16, 3, DEFAULT[3:25]
    login = users.get_current_user()
    name = 'sample' # default
    if login:
      values = {
        'user': login,
        'logout_url': users.create_logout_url(self.request.uri),
      }
      
      lookup = []      
      puzzles = Puzzle.all().filter('user = ', login)
      if puzzles.count():
        for puzzle in puzzles:
          lookup.append(puzzle.name)  
        
        name = self.request.get('name')
        try:
          page = lookup.index(name)
        except:
          page = 0
        
        this = puzzles[page]
        name = this.name
        bank = pickle.loads(this.bank)
        game = pickle.loads(this.game)
        if page < puzzles.count()-1:
          nexturi = '/?name=%s' % lookup[(page + 1)]
        else:
          nexturi = None
          
        if page > 0:
          prevuri = '/?name=%s' % lookup[(page - 1)]
        else:
          prevuri = None

        values.update({'prevuri' : prevuri})
        values.update({'nexturi' : nexturi})
        values.update({'lookup' : lookup})

        rows, cols, secs = this.rows, this.cols, this.secs        
      else:
        game = Crossword(16, 16, '-', 5000, bank)
        game.compute_crossword(3)      

      values.update({'rows' : rows})
      values.update({'cols' : cols})
      values.update({'secs' : secs})
      values.update({'bank' : bank})
    else:
      values = {
        'login_url': users.create_login_url(self.request.uri),
      }
    
      game = Crossword(16, 16, '-', 5000, DEFAULT)
      game.compute_crossword(3)
    
    html = game.html() # we must run these in this order!       
    across, down = game.legend()

    # show/hide editing in template!
    task = self.request.get('task')
    values.update({'task' : task})
    
    values.update({'name' : name or 'sample'})
    values.update({'crossword' : html})
    values.update({'across' : across.split('\n')[:-1]})
    values.update({'down' : down.split('\n')[:-1]})

    path = os.path.join(os.path.dirname(__file__), 'templates/index.html')
    self.response.out.write(template.render(path, values))

  def post(self):
    user = users.get_current_user()
    if not user: return
    
    # meta
    name = self.request.get('name') or 'sample'
    show = self.request.get("show") and True or False

    rows = int(self.request.get('rows', '16')) 
    cols = int(self.request.get('cols', '16')) 
    secs = int(self.request.get('secs', '3')) # time to be spent calculating grid
    hits = int(self.request.get('hits', '0')) + 2 # plus one new (disappearing clue bug)

    # clues   
    bank = []
    for item in range(hits):
      word = self.request.get('word%d' % item)
      clue = self.request.get('clue%d' % item)     
      if word or clue:
        bank.append([word, clue])

    keyname = "%s by %s" % (name, user)
    puzzle = Puzzle.get_or_insert(keyname)

    game = Crossword(rows, cols, '-', 5000, bank)
    game.compute_crossword(secs)
    puzzle.game = pickle.dumps(game)

    puzzle.name = name
    puzzle.user = user
    puzzle.show = show 
    puzzle.rows = rows or 16
    puzzle.cols = cols or 16
    puzzle.secs = secs or 3 #TODO implement delayed via taskqueue (always three now)
    puzzle.bank = pickle.dumps(bank)    
    puzzle.put()
        
    self.redirect('/?' + urllib.urlencode({'name': puzzle.name}))

app = webapp2.WSGIApplication([
    ('/', IndexPageHandler),
    ('/(.*)', StaticPageHandler),
], debug=True)

