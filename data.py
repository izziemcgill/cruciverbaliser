DEFAULT = \
['arithmetic','The science of numbers; the art of computation by figures'], \
['artificial','Made or contrived by art'], \
['attainable','Capable of being reached'], \
['basketball','A ball game, usually played indoors in an area called the court'], \
['blacksmith','He who works in iron with a forge, and makes iron'], \
['boondoggle','a braided cord made by hand by young scouts'], \
['bottleneck','a location or situation in which otherwise rapid progress would be made'], \
['cautionary','Conveying a warning to avoid danger;'], \
['chardonnay','a white wine grape'], \
['condescend','To stoop or descend'], \
['confession','Acknowledgment; avowal'], \
['confidence','The act of trusting, or putting faith in'], \
['conflation','A blowing together, as of many instruments in a concert,'], \
['confluence','The act of flowing together; the meeting or junction of'], \
['conformism','orthodoxy in thoughts and belief'], \
['conformist','marked by conformity or convention; not corresponding to'], \
['conformity','Correspondence in form, manner, or character; resemblance;'], \
['congenital','Arising by birth; having an origin; born'], \
['congestion','The act of gathering into a heap or mass; accumulation'], \
['consistent','Possessing firmness or fixedness; firm; hard; solid'], \
['conspiracy','A combination of people for an evil purpose; an agreement,'], \
['contention','A violent effort or struggle to obtain, or to resist,'], \
['contraband','Illegal or prohibited traffic'], \
['contravene','To meet in the way of opposition; to come into conflict'], \
['contribute','To give a part to a common stock; to lend assistance or'], \
['convection','The act or process of conveying or transmitting'], \
['convenient','Fit or adapted; suitable; proper; becoming; appropriate'], \
['convention','The act of coming together; the state of being together;'], \
['convergent','not parallel; of lines or linear objects'], \
['conversant','Having frequent or customary intercourse; familiary'], \
['conversion','The act of turning or changing from one state or condition'], \
['coordinate','A thing of the same rank with another thing'], \
['cornucopia','The horn of plenty'], \
['cumbersome','Burdensome or hindering, as a weight or drag;'], \
['denouement','The unraveling or discovery of a plot; the catastrophe,'], \
['diabolical','Pertaining to the devil'], \
['diminutive','Below the average size; very small; little'], \
['diplomatic','Pertaining to diplomacy; relating to the foreign ministers'], \
['discipline','The treatment suited to a disciple or learner; education;'], \
['digression','The act of deviating, esp. from the main'], \
['dilapidate','To bring into a condition of decay or partial ruin, by'], \
['conveyance','The act of carrying, or transporting; carriage'], \
['conviction','The act of proving, finding'], \
['saffron', 'The dried, orange yellow plant used to as dye and as a cooking spice'], \
['capricorn', 'The Goat'], \
['fratricide','The act of one who murders or kills his own brother'], \
['fraudulent','Tricky; deceitful; dishonest'], \
['exasperate','To irritate in a high degree; to provoke; to enrage'], \
['evanescent','Liable to vanish or pass away like vapor; vanishing;'], \
['evangelize','To instruct in the gospel; to preach the gospel to'], \
['equestrian','Of or pertaining to horses or horsemen'], \
['eructation','The act of belching wind from the stomach; a belch'], \
['flirtation','Playing at courtship; coquetry'], \
['dreamt', 'Dreams, past'], \
['doxa', 'Greek word for an opinion that may be at least partly true but cannot be fully expounded'], \
['leaven', 'An agent, such as yeast, that cause batter or dough to rise.'], \
['scuba', 'Underwater breathing apparatus'], \
['coda', 'Musical conclusion of a movement or composition'], \
['paladin', 'A heroic champion or paragon of chivalry'], \
['syncopation', 'Shifting the emphasis of a beat to the normally weak beat'], \
['albatross', 'A large bird of the ocean having a hooked beek and long, narrow wings'], \
['harp', 'Musical instrument with 46 or more open strings played by plucking'], \
['piston', 'A solid cylinder or disk that fits snugly in a larger cylinder and moves under pressure as in an engine'], \
['caramel', 'A smooth chery candy made from suger, butter, cream or milk with flavoring'], \
['coral', 'A rock-like deposit of organism skeletons that make up reefs'], \
['dawn', 'The time of each morning at which daylight begins'], \
['pitch', 'A resin derived from the sap of various pine trees'], \
['fjord', 'A long, narrow, deep inlet of the sea between steep slopes'], \
['lip', 'Either of two fleshy folds surrounding the mouth'], \
['lime', 'The egg-shaped citrus fruit having a green coloring and acidic juice'], \
['mist', 'A mass of fine water droplets in the air near or in contact with the ground'], \
['plague', 'A widespread affliction or calamity'], \
['arabesque', 'A pose on one leg with the other leg raised back, the body and arms making a complementary line'], \
['rhythm', 'The element of music pertaining to time'], \
['cognitive', 'Relating to the developmental area surrounding thinking skills'], \
['apogee', 'That point in a terrestrial orbit which is farthest from the Earth'], \
['duet', 'A piece of music written for two'], \
['yarn', 'A strand of twisted threads or a long elaborate narrative'], \
['gotham', 'Batman lives here'], \
['northern', 'Most of Africa lies in this hemisphere'], \
['detroit', 'From this city one can travel south to Canada'], \
['maps', 'What does a cartographer make?'], \
['rommel','World War II commander had the first name Erwin?'], \
['heat', 'Travels by conduction, convection and radiation'], \
['venus', 'The closest planet to Earth'], \
['snicker', 'A snide, slightly stifled laugh'], \
['abandoning','To cast or drive out; to banish; to expel; to reject'], \
['compromise','Concessions so to bind by mutual agreement'], \
['abbreviate','To make briefer; to shorten; to abridge; to reduce by'], \
['abdicating','To surrender or relinquish, as sovereign power'], \
['ruminating','Chewing the cud'], \
['salamander','Newt'], \
['xylography','The art of engraving on wood'], \
['accidental','Happening by chance'], \
['aberration','The act of wandering; deviation, especially from truth'], \
['abolishing','To do away with wholly'], \
['abominable','Worthy of, or causing, abhorrence, as a thing of evil'], \
['aboriginal','An original inhabitant of any land'], \
['quadrupeds','Four-legged'], \
['racecourse','An equine sports venue'], \
['racehorses','Equine sports participants'], \
['like', 'Having the same, or nearly the same, appearance'], \
['her', 'This or that female; the woman understood or referred to'], \
['make', 'To cause to exist; to bring into being; to form; to'], \
['see', 'To perceive by the eye'], \
['number', 'That which admits of being counted or reckoned; a unit'], \
['people', 'The body of persons who compose a community, tribe'], \
['call', 'To command or request to come or be present; to summon'], \
['may', 'The fifth month of the year, containing thirty-one days'], \
['down', 'Fine, soft, hairy outgrowth from the skin or surface of'], \
['side', 'The margin, edge, verge, or border of a surface'], \
['been', 'To exist actually, or in the world of fact; to have'], \
['now', 'At the present time; at this moment'], \
['find', 'To meet with, or light upon, accidentally; to gain the'], \
['any', 'One indifferently, out of an indefinite number; one'], \
['new', 'Having existed, or having been made, but a short time'], \
['work', 'Exertion of strength or faculties; physical'], \
['part', 'To divide; to separate into distinct pieces'], \
['take', 'In an active sense; To lay hold of; to seize with the'], \
['get', 'To procure; to obtain; to gain possession of; to acquire'], \
['place', 'Any portion of space regarded as measured off or distinct'], \
['made', 'To cause to exist; to bring into being; to form'], \
['live', 'Not dead'], \
['where', 'At or in what place; hence, in what situation, position'], \
['after', 'Next; later in time; subsequent; succeeding'], \
['back', 'A large shallow vat; a cistern, tub, or trough'], \
['little', 'Small in size or extent; not big; diminutive'], \
['round', 'To make circular, spherical, or cylindrical'], \
['man', 'Human being'], \
['year', 'The time of the apparent revolution of the sun '], \
['came', 'To move hitherward; to draw near; to approach the speaker'], \
['show', 'The act of bringing to view'], \
['every', 'All the parts which compose a whole collection'], \
['good', 'Possessing desirable qualities; adapted to answer the end'], \
['give', 'To bestow without receiving a return'], \
['under', 'Below or lower, in place or position'], \
['name', 'The title by which any person or thing is known '], \
['through', 'From one end or side to the other'], \
['joust', 'To engage in mock combat on horseback'], \
['form', 'The shape and structure of anything'], \
['sentence', 'To pass or pronounce judgment upon'], \
['great', 'Large in space; of much size; big; immense; enormous'], \
['think', 'To conceive; to imagine'], \
['say', 'To utter or express in words'], \
['help', 'To aid '], \
['low', 'not aloft; not on high'], \
['differ', 'To be or stand apart; to disagree; to be unlike'], \
['turn', 'to revolve'], \
['cause', 'That which produces or effects a result'], \
['much', 'A great quantity; a great deal; also, an indefinite'], \
['mean', 'To have in the mind'], \
['before', 'In front of; preceding in space'], \
['move', 'To cause to change place or posture in any manner'], \
['right', 'That which is correct'], \
['boy', 'A male child, a lad'], \
['old', 'Not young'], \
['too', 'Over; more than enough'], \
['same', 'Not different identical'], \
['tell', 'To give an account; to make report'], \
['set', 'To pass below the horizon'], \
['three', 'The number greater by a unit than two'], \
['want', 'To be without; to be destitute of, or deficient in'], \
['air', 'To expose for the purpose of cooling'], \
['well', 'An issue of water from the earth; a spring; a fountain'], \
['play', 'To engage in sport or lively recreation'], \
['small', 'Having little size, compared with other things of the same'], \
['end', 'To bring to a conclusion'], \
['put', 'To place '], \
['home', 'Dwelling place; the house in which one lives'], \
['hand', 'That part of the fore limb below the forearm or wrist '], \
['port', 'A place where ships may ride secure from storms'], \
['large', 'Exceeding most other things of like kind in bulk'], \
['spell', 'To form words with letters, esp. with the proper letters'], \
['add', 'to increase'], \
['even', 'Level, smooth, or equal in surface'], \
['land', 'To set or put on shore from a ship or other water craft'], \
['here', 'In this place; in the place where the speaker is'], \
['must', 'The expressed juice of the grape, or other fruit'], \
['big', 'Having largeness of size; of much bulk or magnitude'], \
['high', 'Elevated above any starting point of measurement'], \
['such', 'Of that kind'], \
['follow', 'To go or come after; to move behind in the same path'], \
['act', 'That which is done or doing; the exercise of power'], \
['why', 'For what cause, reason, or purpose; on what account'], \
['ask', 'To request; to seek to obtain by words; to petition'], \
['change', 'To alter; to make different'], \
['went', 'To Canterbury they..'], \
['light', 'Having little, or comparatively little, weight'], \
['kind', 'Characteristic of the species; good'], \
['off', 'Denoting distance or separation; as, the house is a mile'], \
['need', 'A state that requires supply or relief; pressing occasion'], \
['house', 'A structure intended or used as a habitation or shelter'], \
['try', 'To endeavor; to make an effort '], \
['again', 'In return, repeated'], \
['animal', 'An organized living being endowed with sensation'], \
['point', 'That which pricks or pierces; the sharp end of anything'], \
['mother', 'A female parent; especially, one of the human race'], \
['world', 'The earth'], \
['near', 'Not far distant in time, place, or degree; not remote'], \
['build', 'To erect or construct'], \
['self', 'The individual'], \
['earth', 'The globe or planet which we inhabit; the world, in'], \
['father', 'A male parent; especially, one of the human race'], \
['stand', 'To endure; to sustain; to bear'], \
['page', 'One side of a leaf of a book or manuscript'], \
['country', 'A tract of land; a region; the territory of an independent'], \
['found', 'To lay the basis of; to set, or place, as on something'], \
['answer', 'To speak or write by way of return'], \
['school', 'A place for learned intercourse and instruction'], \
['grow', 'To increase in size by a natural and organic process'], \
['study', 'A setting of the mind or thoughts upon a subject; hence,'], \
['still', 'Motionless '], \
['learn', 'To gain knowledge or information of'], \
['plant', 'A vegetable'], \
['cover', 'To overspread the surface of (one thing) with another'], \
['food', 'What is fed upon; that which goes to support life '], \
['sun', 'The luminous orb, the light of which constitutes day'], \
['between', 'Betwixt, intermediate. In the space which separates'], \
['state', 'The circumstances or condition of a being or thing '], \
['eye', 'The organ of sight or vision'], \
['never', 'Not ever; not at any time'], \
['last', 'Being after all the others'], \
['thought', 'Of the act of thinking'], \
['city', 'A large town'], \
['tree', 'Any perennial woody plant of considerable size'], \
['cross', 'Vexed '], \
['farm', 'Pertaining to agriculture '], \
['hard', 'Not easily penetrated, cut, or separated into parts'], \
['start', 'To cause to move suddenly; to disturb suddenly'], \
['story', 'A narration or recital of that which has occurred'], \
['saw', 'cut or perceived by the eye'], \
['far', 'not near'], \
['sea', 'One of the larger bodies of salt water'], \
['draw', 'To pull '], \
['left', 'Remaining '], \
['late', 'After the usual or proper time'], \
['run', 'To move swiftly'], \
['while', 'Space of time, or continued duration, esp. when short'], \
['press', 'To urge, or act upon, with force, as weight'], \
['close', 'To stop, or fill up'], \
['night', 'That part of the natural day when the sun is beneath the horizon'], \
['real', 'Actually being or existing; not fictitious or imaginary'], \
['life', 'The state of being which begins with birth'], \
['north', 'That one of the four cardinal points of the compass, at the top'], \
['open', 'Free of access; not shut up'], \
['next', 'Nearest in place; having no similar object intervening'], \
['white', 'Reflecting to the eye all the rays of the spectrum'], \
['children', 'Sons or daughters'], \
['begin', 'To commence'], \
['got', 'To gain possession of '], \
['walk', 'To move along on foot '], \
['example', 'One taken to show the character or quality of'], \
['ease', 'Rest'], \
['paper', 'A substance in the form of thin sheets or leaves'], \
['group', 'A cluster, crowd, or throng'], \
['always', 'At all times'], \
['music', 'The science and the art of tones'], \
['mark', 'A visible sign or impression made or left upon anything'], \
['until', 'To '], \
['river', 'A large stream of water flowing in a bed or channel'], \
['car', 'A small vehicle moved on wheels'], \
['feet', 'The terminal part of the leg of a man or an animal'], \
['second', 'Immediately following the first'], \
['carry', 'To convey or transport in any manner from one place to another'], \
['science', 'Knowledge of principles and causes '], \
['eat', 'To chew and swallow as food'], \
['fish', 'A name loosely applied in popular usage to many animals of the sea'], \
['mountain', 'A large hill '], \
['stop', 'To halt '], \
['once', 'For one time '], \
['hear', 'To perceive by the ear '], \
['horse', 'A hoofed quadruped '], \
['cut', 'A gash'], \
['sure', 'Certainly knowing and believing; confident beyond doubt'], \
['watch', 'Vigil'], \
['color', 'A property depending on the relations of light to the eye,'], \
['wood', 'A large and thick collection of trees; a forest or grove'], \
['enough', 'In a degree or quantity that satisfies'], \
['plain', 'Level land'], \
['young', 'Not long born'], \
['ready', 'Prepared for what one is about to do or experience'], \
['ever', 'At any time; at any period or point of time'], \
['red', 'The color of blood'], \
['feel', 'To perceive by the touch'], \
['talk', 'To speak freely'], \
['soon', 'In a short time'], \
['dog', 'A canine'], \
['family', 'The collective body of persons who live in one house '], \
['direct', 'Straight; not crooked, oblique, or circuitous; leading by'], \
['leave', 'To go away from'], \
['song', 'That which is sung or uttered with musical modulations '], \
['measure', 'The group or grouping of beats'], \
['door', 'An opening in the wall of a house or of an apartment'], \
['black', 'Destitute of light, or incapable of reflecting it'], \
['wind', 'To turn completely, or with repeated turns'], \
['complete', 'Filled up; with no part or element lacking'], \
['ship', 'Any large seagoing vessel'], \
['rock', 'To cause to sway backward and forward'], \
['piece', 'A fragment or part of anything separated from the whole,'], \
['pass', 'An opening, road, or track'], \
['top', 'A toy. The highest part of anything'], \
['whole', 'The entire thing '], \
['king', 'A chief ruler; a sovereign'], \
['space', 'Extension, considered independently of anything which it'], \
['heard', 'Perceived by the ear'], \
['hour', 'The twenty-fourth part of a day; sixty minutes'], \
['remember', 'To have (a notion or idea) come into the mind again'], \
['step', 'To move the foot in walking; to advance or recede by'], \
['early', 'In advance of the usual or appointed time'], \
['west', 'The point where the sun is seen to set'], \
['ground', 'Reduced to powder, as in a mill'], \
['interest', 'To engage the attention of'], \
['reach', 'To stretch out the hand'], \
['fast', 'To abstain from food'], \
['listen', 'To give close attention with the purpose of hearing'], \
['travel', 'To journey'], \
['less', 'A smaller portion or quantity'], \
['morning', 'The first or early part of the day'], \
['simple', 'Single; not complex'], \
['war', 'A contest between nations or states, carried on by force'], \
['lay', 'To produce and deposit eggs'], \
['slow', 'Moving a short space in a relatively long time'], \
['center', 'A point equally distant from the extremities'], \
['love', 'To regard with affection'], \
['money', 'A piece of metal, as gold, silver, copper, etc'], \
['serve', 'To work for someone else'], \
['appear', 'To become visible'], \
['map', 'A representation of the surface of the earth'], \
['rain', 'To fall in drops from the clouds as water'], \
['rule', 'To have power or command; to exercise supreme authority'], \
['govern', 'To direct and control '], \
['pull', 'To draw, or attempt to draw, toward one'], \
['cold', 'Deprived of heat'], \
['notice', 'To observe '], \
['voice', 'Sound uttered by the mouth'], \
['fly', 'To move in or pass through the air with wings'], \
['fall', 'To descend, either suddenly or gradually'], \
['dark', 'Absence of light'], \
['wait', 'To stay for; to rest or remain stationary in expectation'], \
['star', 'One of the innumerable luminous bodies seen in the sky'], \
['rest', 'That which is left, or which remains'], \
['correct', 'To make right '], \
['pound', 'To strike heavy blows; to beat'], \
['beauty', 'An assemblage of graces or properties pleasing to the eye'], \
['drive', 'To impel or urge onward by force in a direction away from'], \
['teach', 'To impart the knowledge of'], \
['final', 'Pertaining to the end or conclusion; last; terminating'], \
['minute', 'The sixtieth part of an hour; Very small'], \
['strong', 'Having great physical power'], \
['clear', 'transparent '], \
['street', 'A paved way or road'], \
['nothing', 'Not anything '], \
['wheel', 'A circular frame turning about an axis; a rotating disk'], \
['full', 'Filled up '], \
['moon', 'The celestial orb which revolves round the earth'], \
['island', 'A tract of land surrounded by water'], \
['busy', 'Hard at work '], \
['boat', 'A small open vessel, or water craft'], \
['check', 'A word of warning denoting that the king is in trouble '], \
['game', 'Sport of any kind; jest, frolic'], \
['miss', 'A title of courtesy prefixed to the name of a girl'], \
['heat', 'To make hot '], \
['ball', 'Any round or roundish body or mass; a sphere or globe'], \
['wave', 'An advancing ridge or swell on the surface of a liquid'], \
['drop', 'The quantity of fluid which falls in one small spherical part'], \
['heavy', 'Weighty '], \
['material', 'Consisting of matter '], \
['ice', 'Water or other fluid frozen or reduced to the solid state'], \
['pick', 'To eat slowly, sparingly, or by morsels'], \
['sudden', 'Happening without previous notice '], \
['length', 'The longest, or longer, dimension of any object '], \
['energy', 'Internal or inherent power'], \
['hunt', 'The act or practice of chasing wild animals'], \
['bed', 'An article of furniture to sleep or take rest in or on'], \
['brother', 'A male person who has the same father and mother'], \
['cell', 'A very small and close apartment, as in a prison'], \
['forest', 'An extensive wood'], \
['window', 'An opening in the wall of a building'], \
['lone', 'Being without a companion'], \
['wall', 'A work or structure of stone, brick, or other materials'], \
['catch', 'To lay hold on; to seize, especially with the hand'], \
['mount', 'To get upon; to ascend'], \
['wish', 'Desire; eager desire; longing'], \
['cow', 'The mature female of bovine animals'], \
['million', 'The number of ten hundred thousand'], \
['bear', 'To support or sustain; to hold up'], \
['flower', 'In the popular sense, the bloom or blossom of a plant'], \
['clothe', 'To put garments on; to dress'], \
['jump', 'To spring free from the ground by the muscular action of'], \
['hair', 'The collection or mass of filaments growing from the skin'], \
['burn', 'To consume with fire'], \
['hill', 'A natural elevation of land'], \
['safe', 'Free from harm, injury, or risk'], \
['silent', 'Free from sound or noise'], \
['tall', 'High in stature'], \
['sand', 'Fine particles of stone'], \
['soil', 'To make dirty or unclean on the surface'], \
['fight', 'A battle'], \
['lie', 'A falsehood uttered or acted for the purpose of deception'], \
['kill', 'To deprive of life, animal or vegetable'], \
['son', 'A male child; the male issue, or offspring, of a parent'], \
['moment', 'A minute portion of time'], \
['loud', 'Having, making, or being a strong or great sound; noisy'], \
['spring', 'To leap; to bound; to jump'], \
['child', 'A son or a daughter; a male or female descendant '], \
['dictionary', 'A book containing the words of a language'], \
['method', 'An orderly procedure or process'], \
['cloud', 'A collection of visible vapor, or watery particles,'], \
['poor', 'Destitute of property; wanting in material riches'], \
['experiment', 'A trial or special observation'], \
['hole', 'A hollow place or cavity; an excavation; a pit; an opening'], \
['trade', 'To barter, or to buy and sell'], \
['melody', 'A sweet or agreeable succession of sounds'], \
['trip', 'To cause to stumble'], \
['row', 'To propel with oars, as a boat or vessel'], \
['exact', 'Precisely agreeing with a standard, a fact, or the truth'], \
['symbol', 'A visible sign or representation of anything'], \
['die', 'A small cube, marked on its faces with spots from one to'], \
['clean', 'Free from dirt or filth'], \
['blood', 'The fluid which circulates in the body'], \
['touch', 'To come in contact with; to hit or strike lightly against'], \
['wire', 'A thread or slender rod of metal'], \
['cost', 'The amount paid, charged, or engaged to be paid for'], \
['lost', 'To part with unintentionally or unwillingly'], \
['sent', 'To cause to go in any manner'], \
['choose', 'To make a selection; to decide'], \
['bank', 'A mound, pile, or ridge of earth, raised above the river'], \
['save', 'To make safe'], \
['difficult', 'Hard to do'], \
['noon', 'The middle of the day; midday'], \
['locate', 'To place; to set in a particular spot or position'], \
['character', 'A distinctive mark; a letter, figure, or symbol'], \
['history', 'Ancient facts'], \
['student', 'A person engaged in study; one who is devoted to learning'], \
['corner', 'The point where two converging lines meet'], \
['bone', 'The hard, calcified tissue of the skeleton'], \
['soldier', 'One who is engaged in military service'], \
['create', 'To bring into being; to form out of nothing'], \
['neighbour', 'A person who lives near another'], \
['wash', 'To cleanse by dipping or rubbing in water'], \
['bat', 'A large stick; a club; specifically, a piece of wood'], \
['crowd', 'To press together or collect in numbers; to swarm; to'], \
['corn', 'A single seed of certain plants, as wheat, rye, barley,'], \
['compare', 'To examine the character or qualities of, as of two '], \
['poem', 'A metrical composition in verse'], \
['string', 'A small cord, a line, a twine'], \
['tube', 'A hollow cylinder'], \
['fear', 'To be afraid of'], \
['triangle', 'A figure bounded by three lines '], \
['hurry', 'To hasten; to impel to greater speed; to urge on'], \
['chief', 'The head or leader of any body of men; a commander'], \
['colony', 'A company of people transplanted from their mother country'], \
['clock', 'A machine for measuring time'], \
['mine', 'To dig a pit in the earth; to get ore, metals'], \
['enter', 'To come or go into'], \
['major', 'An officer next in rank above a captain'], \
['fresh', 'Possessed of original life and vigor; new and strong'], \
['search', 'To look over or through, for the purpose of finding'], \
['yellow', 'Being of a bright saffronlike color; of the color of gold'], \
['dead', 'Deprived of life'], \
['desert', 'To leave (especially something which one should stay by'], \
['suit', 'To fit '], \
['lift', 'To try to raise something'], \
['block', 'To obstruct so as to prevent passage or progress'], \
['shoe', 'A covering for the human foot, usually made of leather'], \
['camp', 'The ground or spot on which tents, huts, etc., are erected'], \
['gather', 'To bring together; to collect'], \
['stretch', 'To reach out; to extend'], \
['throw', 'To fling, cast, or hurl'], \
['shine', 'To emit rays of light'], \
['column', 'A kind of pillar'], \
['repeat', 'To go over again'], \
['broad', 'Wide; extend in breadth, or from side to side'], \
['prepare', 'To fit, adapt, or qualify for a particular purpose '], \
['salt', 'The chloride of sodium, a substance used for seasoning'], \
['woman', 'An adult female person; a grown-up female person'], \
['season', 'One of the divisions of the year'], \
['fig', 'A small fruit tree'], \
['sister', 'A female who has the same parents with another person'], \
['similar', 'Exactly corresponding; resembling in all respects'], \
['score', 'To mark with lines, scratches, or notches'], \
['apple', 'The fleshy pome or fruit of a rosaceous tree'], \
['pitch', 'To fix or place a tent or temporary habitation; to encamp'], \
['coat', 'An outer garment fitting the upper part of the body'], \
['mass', 'A quantity of matter cohering together so as to make one'], \
['win', 'To gain by superiority in competition or contest'], \
['dream', 'To have ideas or images in the mind while in the state of sleep'], \
['evening', 'The latter part and close of the day'], \
['feed', 'To give food to; to supply with nourishment; to satisfy'], \
['valley', 'The space inclosed between ranges of hills or mountains'], \
['double', 'Twofold; multiplied by two; increased by its equivalent'], \
['post', 'A piece of timber, metal, or other solid substance, fixed,'], \
['glad', 'Pleased; joyous; happy; cheerful; gratified'], \
['bread', 'An article of food made from flour'], \
['slave', 'A person who is held in bondage to another'], \
['duck', 'To go under the surface of water and immediately reappear'], \
['drink', 'To swallow anything liquid, for quenching thirst or otherwise'], \
['speech', 'The faculty of uttering articulate sounds or words'], \
['nature', 'The existing system of things; the universe of matter'], \
['path', 'A footway'], \
['liquid', 'Flowing freely like water; fluid; not solid'], \
['log', 'A bulky piece of wood which has not been shaped by hewing'], \
['shell', 'A hard outside covering, as of a fruit or an animal'], \
['neck', 'The part of an animal which connects the head and the body'],
